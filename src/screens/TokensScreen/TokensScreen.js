// @flow

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  View,
  ScrollView,
  Alert,
  Image
} from 'react-native';
import {
  Text
} from 'react-native-elements';
import { Navigation } from 'react-native-navigation';
import { connectWallet } from 'src/redux';
import { getTokens, base64ToHex } from 'src/utils/WalletUtils';
import Config from 'react-native-config';

const styles = StyleSheet.create({
  flex: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start'
  },
  basicContainer: {
    padding: 10,
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: 'white',
    width: '100%',
    alignSelf: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  buttonsContainer: {
    flexDirection: 'row',
    alignContent: 'space-between',
  },
  buttonIn: {
    backgroundColor: '#ef3b23',
    borderRadius: 25,
  },
  buttonOut: {
    color: '#ef3b23',
    borderRadius: 25,
  },
});

class TokensScreen extends PureComponent {

  constructor(props) {
    super(props);

    Navigation.events().bindComponent(this);
  }

  componentDidAppear() {
    const { setWalletValues, timestamp } = this.props;
    const { addresses, receiveAddress } = this.props.wallet[timestamp];

    getTokens(global.socketConnect, addresses).then((tokens) => {
      setWalletValues({tokens: tokens, timestamp: timestamp});
    })
  }

  render() {
      const { timestamp } = this.props;

      if (!(timestamp in this.props.wallet)) {
          return <View/>;
      }

      const { tokens } = this.props.wallet[timestamp];

      return (
          <View style={[styles.flex, {flexDirection: 'column', justifyContent: 'center', alignSelf: 'center', width: '100%'}]}>
            {tokens.length == 0 && <View style={{alignSelf: 'center', marginTop: 10, flex: 1, justifyContent: 'flex-end'}}>
              <Text style={{textAlign: 'center', color: 'gray'}}>{ global.strings['tokens.empty'] }</Text>
            </View>}
            <ScrollView style={{width: '100%'}}>
              {
                tokens.map((token, key) => (
                  <TokenItem params={token} key={key} />
                ))
              }
            </ScrollView>
          </View>
      );
  }
}

class TokenItem extends PureComponent {
  render = () => {
    const { params } = this.props;

    return (
        <View style={styles.basicContainer}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <View style={{width: 50, height: 50, marginRight: 10, borderRadius: 25, justifyContent: "center", alignItems: "center", backgroundColor: "#333"}}>
              <Text style={{color: "white", fontSize: 14, fontWeight: 'bold'}}>
                {params.tokenName}
              </Text>
            </View>
            <View style={{flexDirection: 'column', justifyContent: 'center', flex: 1}}>
              <Text style={{fontWeight: 'bold', color: 'black', fontSize: 14}}>{params.tokenName}</Text>
              <Text style={{fontSize: 14, marginTop: 5}}>{(params.balance/Math.pow(10, params.units)).toFixed(params.units)} <Text style={{opacity: 0.5}}>({(params.locked/Math.pow(10, params.units)).toFixed(params.units)})</Text> {params.tokenName}</Text>
            </View>
          </View>
        </View>
    );
  }
}

TokensScreen.propTypes = {
  wallet: PropTypes.shape({}).isRequired
};

export default connectWallet()(TokensScreen);
