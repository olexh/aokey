import bip39 from 'bip39';
import * as bip32 from 'bip32';
import HDKey from 'hdkey';
import moment from "moment";
import * as bitcoin from "bitcoinjs-lib";
import * as aes256 from "aes256";
import Config from 'react-native-config';
import AsyncStorage from '@react-native-community/async-storage';

const hashType = bitcoin.Transaction.SIGHASH_ALL;
const aok = {
	messagePrefix: '\x19AOK Signed Message:\n',
	bip32: {
		public: 76066276,
   		private: 76067358
	},
	// bech32: 'sugar',
	pubKeyHash: parseInt(Config.PUB_KEY_HASH),
	scriptHash: parseInt(Config.SCRIPT_HASH),
	wif: parseInt(Config.WIF),
	dustThreshold: 0
}

export function numberWithCommas(number) {
	var parts = number.toString().split(".");
	parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");

	return parts.join(".");
}


export const base64ToHex = ( () => {
   // Lookup tables
   const values = [], output = [];

   // Main converter
   return function base64ToHex ( txt, sep = '' ) {
      if ( output.length <= 0 ) populateLookups();
      const result = [];
      let v1, v2, v3, v4;
      for ( let i = 0, len = txt.length ; i < len ; i += 4 ) {
         // Map four chars to values.
         v1 = values[ txt.charCodeAt( i   ) ];
         v2 = values[ txt.charCodeAt( i+1 ) ];
         v3 = values[ txt.charCodeAt( i+2 ) ];
         v4 = values[ txt.charCodeAt( i+3 ) ];
         // Split and merge bits, then map and push to output.
         result.push(
            output[ ( v1 << 2) | (v2 >> 4) ],
            output[ ((v2 & 15) << 4) | (v3 >> 2) ],
            output[ ((v3 &  3) << 6) |  v4 ]
         );
      }
      // Trim result if the last values are '='.
      if ( v4 === 64 ) result.splice( v3 === 64 ? -2 : -1 );
      return result.join( sep );
   };

   function populateLookups () {
      const keys = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
      for ( let i = 0 ; i < 256 ; i++ ) {
         output.push( ( '0' + i.toString( 16 ) ).slice( -2 ) );
         values.push( 0 );
      }
      for ( let i = 0 ; i <  65 ; i++ )
         values[ keys.charCodeAt( i ) ] = i;
   }
} )();

export function encryptData(data, key) {
    if(key) {
        return aes256.encrypt(key, data);
    }

    return null;
}

export function decryptData(data, key) {
    if(key) {
        return aes256.decrypt(key, data);
    }

    return null;
}

export function decryptWallet(data, password) {
	let wallet = {};

	try {
		wallet.seedPhrase = decryptData(data.mnemonicPhrase, password).split(" ");
		wallet.title = data.title;
		wallet.addresses = {};

		for (address in data.addresses.internal) {
			wallet.addresses[address] = {index: 0, privateKey: decryptData(data.addresses.internal[address].privateKey, password)};
		}

		for (address in data.addresses.external) {
			wallet.addresses[address] = {index: 0, privateKey: decryptData(data.addresses.external[address].privateKey, password)};
		}

		wallet.receiveAddress = data.addresses.currentExternal;
		wallet.isCreated = true;
		wallet.isMigrated = true;
	} catch (e) {
		alert(e);
		return null;
	}

	return wallet;
}

export async function getMirgationWallets() {
  try {
    const wallets = await AsyncStorage.getItem('wallets');

    if(wallets !== null) {
    	AsyncStorage.setItem('wallets_backup', wallets);
      	return JSON.parse(wallets);
    }
  } catch(e) {
    return null;
  }

  return null;
}

export async function removeMirgationWallets() {
  try {
    await AsyncStorage.removeItem('wallets');
    return true;
  } catch(e) {
    return false;
  }

  return false;
}

function cltvOutput(address, lockTime) {
	return bitcoin.script.compile([
	  bitcoin.script.number.encode(lockTime),
	  bitcoin.opcodes.OP_CHECKLOCKTIMEVERIFY,
	  bitcoin.opcodes.OP_DROP,
	  bitcoin.opcodes.OP_DUP,
	  bitcoin.opcodes.OP_HASH160,
	  bitcoin.address.fromBase58Check(address, aok).hash,
	  bitcoin.opcodes.OP_EQUALVERIFY,
	  bitcoin.opcodes.OP_CHECKSIG
	])
}

function writeUInt64LE(buffer, value, offset) {
  buffer.writeInt32LE(value & -1, offset);
  buffer.writeUInt32LE(Math.floor(value / 0x100000000), offset + 4);
  return offset + 8;
}

function tokenOutput(address, token, locktime = 0) {
	var tokenBuffer = Buffer.allocUnsafe(3+1+1+ token.name.length +8+4);
	var offset = 0;

	tokenBuffer.write("alp", offset);
	offset += 3;
	tokenBuffer.write("t", offset);
	offset += 1;
	tokenBuffer.writeUInt8(token.name.length, offset);
	offset += 1;
	tokenBuffer.write(token.name, offset);
	offset += token.name.length;
	writeUInt64LE(tokenBuffer, token.amount, offset);
	offset += 8;

	if (locktime) {
		tokenBuffer.writeInt32LE(locktime, offset);
	}

	return bitcoin.script.compile([
	  bitcoin.opcodes.OP_DUP,
	  bitcoin.opcodes.OP_HASH160,
	  bitcoin.address.fromBase58Check(address, aok).hash,
	  bitcoin.opcodes.OP_EQUALVERIFY,
	  bitcoin.opcodes.OP_CHECKSIG,
	  bitcoin.opcodes.OP_ALP_TOKEN,
	  tokenBuffer,
	  bitcoin.opcodes.OP_DROP,
	])
}

function getAddress(node, network) {
	return bitcoin.payments.p2pkh({ pubkey: node.publicKey, network }).address;
}

function removeDuplicates(myArr, prop) {
    return myArr.filter((obj, pos, arr) => {
        return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos;
    });
}
export function validateMnemonic(mnemonic) {
    return bip39.validateMnemonic(mnemonic)
}

export function getSeed(mnemonic) {
    return bip39.mnemonicToSeed(mnemonic);
}

export function getHDKey(seed) {
    return HDKey.fromMasterSeed(seed);
}

export function isAddress(address) {
	try {
    	bitcoin.address.toOutputScript(address, aok)
		return true
	} catch (e) {
		return false
	}
}

export function importAddressByWIF(wif) {
	try {
		const keyPair = bitcoin.ECPair.fromWIF(wif, aok);
		return getAddress(keyPair, aok);
	} catch (e) {
		return null;
	}
}

export function getLanguageInfo(code) {
	const isolangs = require('assets/isolangs.json');

	if (code in isolangs) {
		return isolangs[code];
	} else {
		return null;
	}
}

export function generateSeedPhrase(size = 12) {
	const mnemonic = require('assets/mnemonics.json');
	let seedPhrase = [];
	let randomNumbers = [];

	for (var i = 0; i < size; i++) {
	  while (true) {
	    let num = Math.floor(Math.random()*mnemonic.words.length);

	    if (!randomNumbers.includes(num)) {
	      randomNumbers.push(num);
	      seedPhrase.push(mnemonic.words[num]);
	      break;
	    }
	  }
	}

	return seedPhrase;
}

export function generateAddresses(seedPhrase, derivePath = "m/44'/0'/0'/0", startIndex = 0, endIndex = 0) {
    const seed = bip39.mnemonicToSeed(seedPhrase);
	const root = bip32.fromSeed(seed, aok);
    let addressList = {};
    let promises = [];

    for (var i = startIndex; i <= endIndex; i++) {
		const child = root.derivePath(derivePath + "/" + i.toString());
	    addressList[getAddress(child, aok)] = {index: i, privateKey: child.toWIF()};
    }

    return addressList;
}

export async function checkAddresses(socketConnect, addresses) {
    var addressList = {};

	var result = await socketConnect.check_addresses(Object.keys(addresses));
	console.log("socketConnect.check_addresses", result)

	if (Array.isArray(result)) {
		for(var i = 0; i < result.length; i++) {
			addressList[result[i]] = addresses[result[i]];
		}
	}

    return addressList;
}

export async function findAddresses(socketConnect, seedPhrase, derivePath) {
    let k = 0;
    let checkMore = true;
    let addressList = {};
    let findedAddressList = {};

    console.log("searching for addresses...")

    while (checkMore) {
        checkMore = false;

        console.log("generating...", 0+k, 19+k)
        addressList = await generateAddresses(seedPhrase, derivePath, 0+k, 19+k);
        console.log("searching...", 0+k, 19+k)
        addressList = await checkAddresses(socketConnect, addressList);

        if (Object.keys(addressList).length > 0) {
            k += 20;
            checkMore = true;
        }

        findedAddressList = {...findedAddressList, ...addressList};
    }

    // alert(JSON.stringify(findedAddressList))
    return findedAddressList;
}

export async function sendTransation(socketConnect, walletAddresses, mainAddress, recieveAddress, amount, fee, timelock = 0, tokenName = "") {
	var outputsAmount = 0;
	var tokenOutputsAmount = 0;
	var keyPairs = [];
	var scripts = [];
    const txb = new bitcoin.TransactionBuilder(aok)
    const timestamp = parseInt(Date.now() / 1000);

    txb.setVersion(1);
	txb.setTimestamp(timestamp);

	if (tokenName.length) {
		for (var address in walletAddresses) {
			var tokens = null;

			try {
				tokens = (await socketConnect.get_balance(address)).tokens;
			} catch (e) {
				console.log(e);
			}

			for (var i = 0; i < tokens.length; i++) {
				if (tokens[i].tokenName == tokenName) {
					if (tokens[i].received != null && (tokens[i].received > 0)) {
						try {
							let utxo = await socketConnect.get_unspent(address, tokens[i].received, tokens[i].tokenName);
							console.log("utxo:", utxo)
							if (utxo.length == 0) {
								let utxo = await socketConnect.get_unspent(address, 0, tokens[i].tokenName);
							}

							for (var k = 0; k < utxo.length; k++) {
								if (parseInt(tokenOutputsAmount-amount) > 0) {
									break;
								}

								txb.addInput(utxo[k].txid, utxo[k].index);

								// console.log(utxo[k])

								scripts.push(new Buffer(utxo[k].script, 'hex'));
								keyPairs.push(bitcoin.ECPair.fromWIF(walletAddresses[address].privateKey, aok));

								console.log("tokenOutputsAmount", tokenOutputsAmount, utxo[k].value)
								tokenOutputsAmount += parseInt(utxo[k].value);
							}
						} catch (e) {
							console.log(e);
						}
					}
				}
			}
		}
	}

	for (var address in walletAddresses) {
		var balance = null;

		try {
			balance = await socketConnect.get_balance(address);
		} catch (e) {
			console.log(e);
		}

		if (balance != null && (balance.balance > 0)) {
			try {
				let utxo = await socketConnect.get_unspent(address);

				for (var k = 0; k < utxo.length; k++) {

					if (tokenName.length) {
						if (parseInt(outputsAmount-fee) > 0) {
							break;
						}
					} else {
						if (parseInt(outputsAmount-amount-fee) > 0) {
							break;
						}
					}

					var decodedScript = bitcoin.script.decompile(new Buffer(utxo[k].script, 'hex'));

					if (decodedScript.includes(bitcoin.opcodes.OP_CHECKLOCKTIMEVERIFY)) {
						if (Buffer.isBuffer(decodedScript[0])) {
							if (decodedScript[0].readUIntLE(0, decodedScript[0].length) <= 50000000) {
								txb.setLockTime((await socketConnect.get_info()).blocks);
							} else {
								txb.setLockTime((await socketConnect.get_info()).mediantime);
							}
						} else {
							txb.setLockTime((await socketConnect.get_info()).blocks);
						}

						txb.addInput(utxo[k].txid, utxo[k].index, 0xfffffffe);
					} else {
						txb.addInput(utxo[k].txid, utxo[k].index);
					}

					scripts.push(new Buffer(utxo[k].script, 'hex'));
					keyPairs.push(bitcoin.ECPair.fromWIF(walletAddresses[address].privateKey, aok));

					outputsAmount += parseInt(utxo[k].value);
				}
			} catch (e) {
				console.log("here", e);
			}
		}
	}

	if (outputsAmount < fee) {
		return {error: "Output amount error"};
	}

	try {
			if (tokenName.length) {
				if (tokenOutputsAmount-amount > 0) {

					if (mainAddress != null) {
						txb.addOutput(tokenOutput(mainAddress, {name: tokenName, amount: parseInt(tokenOutputsAmount-amount)}), 0);
					} else {
						return {error: "Main address error"};
					}
				}

				if (!isNaN(timelock) && timelock > 0) {
					txb.addOutput(tokenOutput(recieveAddress, {name: tokenName, amount: amount}, timelock), 0);
				} else {
					txb.addOutput(tokenOutput(recieveAddress, {name: tokenName, amount: amount}), 0);
				}

				if (outputsAmount-fee > 0) {
					if (mainAddress != null) {
						txb.addOutput(mainAddress, parseInt(outputsAmount-fee));
					} else {
						return {error: "Main address error"};
					}
				}

			} else {
				if (outputsAmount-amount-fee > 0) {
					if (mainAddress != null) {
						txb.addOutput(mainAddress, parseInt(outputsAmount-amount-fee));
					} else {
						return {error: "Main address error"};
					}
				}

				if (!isNaN(timelock) && timelock > 0) {
					txb.addOutput(cltvOutput(recieveAddress, timelock), parseInt(amount));
				} else {
					txb.addOutput(recieveAddress, parseInt(amount));
				}
		}
	} catch (e) {
		return {error: e.message}
	}

	// console.log(txb)

	const tx = txb.buildIncomplete()

	console.log(tx)

	for (var i = 0; i < keyPairs.length; i++) {
		const signatureHash = tx.hashForSignature(i, scripts[i], hashType);

		tx.setInputScript(i, bitcoin.script.compile([
          bitcoin.script.signature.encode(keyPairs[i].sign(signatureHash), hashType),
          keyPairs[i].publicKey
        ]));
	}

	try {
		let txHex = tx.toHex();
		console.log(txHex)
		let broadcast = await socketConnect.broadcast_transaction(txHex)

		return {tx: broadcast};
	} catch (e) {
		return {error: e.message}
	}

}

export async function subscribeToAddresses(socketConnect, walletAddresses, callback) {
	socketConnect.socket.on('address.update', (res) => {
		createTransactionsFromHistory(socketConnect, walletAddresses, res.result.tx).then((newMempoolObjects) => {
			callback(newMempoolObjects, false);
		})
	});

	for (var address in walletAddresses) {
		await socketConnect.subscribe_address(address);
	}
}

export async function createTransactionObject(socketConnect, walletAddresses, transactionVerbose) {
	let transaction = {};
	let isCoinbase = false;

	transaction.hash = transactionVerbose.txid;
	transaction.confirmations = transactionVerbose.confirmations;
	transaction.amount = 0;
	transaction.lock = {};
	transaction.time = transactionVerbose.time;
	transaction.token = {};
	transaction.fee = 0;
	transaction.type = 0;

	for (var k = 0; k < transactionVerbose.vin.length; k++) {
		if ("coinbase" in transactionVerbose.vin[k]) {
			transaction.from = "coinbase";
			isCoinbase = true;
			continue;
		}

		if (Object.keys(walletAddresses)
		      	 .includes(transactionVerbose.vin[k].scriptPubKey.addresses[0]) ) {

			if (transactionVerbose.vin[k].scriptPubKey.type == "transfer_token") {
				if (Object.keys(transaction.token).length != 0 && transaction.token.name == transactionVerbose.vin[k].scriptPubKey.token.name) {
					transaction.token.amount += transactionVerbose.vin[k].scriptPubKey.token.amount;
				} else {
					transaction.token = transactionVerbose.vin[k].scriptPubKey.token;
				}
			}

			transaction.amount += transactionVerbose.vin[k].value;
			transaction.type = 1;
			transaction.from = transactionVerbose.vin[k].scriptPubKey.addresses[0];
		}

		transaction.fee += (!isCoinbase) && transactionVerbose.vin[k].value;
	}

	if (transaction.amount == 0) {
		transaction.type = 0;

		for (var k = 0; k < transactionVerbose.vout.length; k++) {
			if (transactionVerbose.vout[k].scriptPubKey.type != "nonstandard") {
				if (Object
						.keys(walletAddresses)
						.includes(transactionVerbose.vout[k].scriptPubKey.addresses[0])) {

					if (transactionVerbose.vout[k].scriptPubKey.type == "transfer_token") {
						if (Object.keys(transaction.token).length != 0 && transaction.token.name == transactionVerbose.vout[k].scriptPubKey.token.name) {
							transaction.token.amount += transactionVerbose.vout[k].scriptPubKey.token.amount;
						} else {
							transaction.token = transactionVerbose.vout[k].scriptPubKey.token;
						}
					}

					transaction.amount += transactionVerbose.vout[k].value;
					transaction.to = transactionVerbose.vout[k].scriptPubKey.addresses[0];

					if (transactionVerbose.vout[k].scriptPubKey.type == "cltv") {
						transaction.lock[transactionVerbose.vout[k].scriptPubKey.asm.split(" ")[0]] = transactionVerbose.vout[k].value;
					}

				} else {
					if (!isCoinbase) {
						transaction.from = transactionVerbose.vout[k].scriptPubKey.addresses[0];
					}

				}

				transaction.fee -= (!isCoinbase) && transactionVerbose.vout[k].value;
			}

		}
	} else {
		for (var k = 0; k < transactionVerbose.vout.length; k++) {
			if (transactionVerbose.vout[k].scriptPubKey.type != "nonstandard") {
				if (Object
						.keys(walletAddresses)
						.includes(transactionVerbose.vout[k].scriptPubKey.addresses[0])) {

					if (transactionVerbose.vout[k].scriptPubKey.type == "transfer_token") {
							if (Object.keys(transaction.token).length != 0 && transaction.token.name == transactionVerbose.vout[k].scriptPubKey.token.name) {
								transaction.token.amount -= transactionVerbose.vout[k].scriptPubKey.token.amount;
							} else {
								transaction.token = transactionVerbose.vout[k].scriptPubKey.token;
							}
					}

					transaction.amount -= transactionVerbose.vout[k].value;

					if (transactionVerbose.vout[k].scriptPubKey.type == "cltv") {
						transaction.lock[transactionVerbose.vout[k].scriptPubKey.asm.split(" ")[0]] = transactionVerbose.vout[k].value;
					}
				} else {
					transaction.to = transactionVerbose.vout[k].scriptPubKey.addresses[0];
				}

				transaction.fee -= (!isCoinbase) && transactionVerbose.vout[k].value;
			}

		}

		if (!("to" in transaction)) {
			transaction.to = transaction.from;
		}

		if (transaction.amount < 0) {
			transaction.type = 0;
			transaction.amount = transaction.amount * (-1)
		}

		if (transaction.token.amount < 0) {
			transaction.token.amount = transaction.token.amount * (-1)
		}

	}

	if (transactionVerbose.timestamp != null) {
		transaction.date = transactionVerbose.timestamp;
	} else {
		transaction.date = null;
	}

	return transaction;
}

async function createTransactionsFromHistory(socketConnect, walletAddresses, history) {
	var transactionHistory = {};
	var promises = [];

	if (history.length == 0) {
		return transactionHistory;
	}

	var transactions = await socketConnect.get_transaction_batch(history);

	for (var i = 0; i < transactions.length; i++) {
		promises.push(createTransactionObject(socketConnect, walletAddresses, transactions[i].result).then((transaction) => {
			console.log("createTransactionObject.transaction", transaction);
			if (transaction.date) {
				transactionHistory[transaction.date + transaction.hash] = transaction;
			}
		}));
	}

	await Promise.all(promises);
	return transactionHistory;
}

export async function getTransactionHistory(socketConnect, walletAddresses, transactions = null) {
	var allHistory = [];

	for (let address in walletAddresses) {
		await socketConnect.get_history(address).then((history) => {
			allHistory.push.apply(allHistory, history.tx);
		})
	}

	allHistory = allHistory.filter(function(item, pos) {
	    return allHistory.indexOf(item) == pos;
	});

	if (transactions != null) {
		for (var time in transactions) {
			for (var i = 0; i < allHistory.length; i++) {
				if (allHistory[i] == transactions[time].hash && transactions[time].confirmations > 6) {
					allHistory.splice(i, 1);
					break;
				}
			}
		}
	}

	return createTransactionsFromHistory(socketConnect, walletAddresses, allHistory);
}

export async function estimateFee () {
	return "0.00001";
}

export async function getBalance(transactions) {
   	let balance = {"confirmed": 0, "unconfirmed": 0};
	let currentTimestamp = parseInt(Date.now() / 1000);
   	let currentHeight = (await socketConnect.get_info()).blocks;

	for (time in transactions) {
		if (transactions[time].type) {
			balance.confirmed -= transactions[time].amount;
		} else {
			balance.confirmed += transactions[time].amount;
		}

	    for (var locktime in transactions[time].lock) {
	    	if (locktime > 50000000 && locktime > currentTimestamp) {
		    	balance.unconfirmed += transactions[time].lock[locktime];
		    } else if (locktime <= 50000000 && locktime > currentHeight) {
		    	balance.unconfirmed += transactions[time].lock[locktime];
		    }
	    }
    }

    balance.confirmed -= balance.unconfirmed;
    return balance;
}

export async function getTokens (socketConnect, walletAddresses) {
    let promises = [];
    let tokens = [];
    let result = [];

    for (let address in walletAddresses) {
        promises.push(socketConnect.get_balance(address).then((result) => {
            if (result != null) {
    			tokens = tokens.concat(result.tokens);
            }
        }))
    }

    await Promise.all(promises);

    for (var i = 0; i < tokens.length; i++) {
		tokens[i].tokenHash = bitcoin.crypto.hash256(tokens[i].tokenName).toString('hex');
	}

	// for (var i = 0; i < tokens.length; i++) {
	// 	for (var j = 0; j < result.tokens.length; j++) {
	// 		if (result[j].tokenName == tokens[i].tokenName) {
	// 			result[j].received += tokens[i].received;
	// 			result[j].locked += tokens[i].locked;
	// 			result[j].balance += tokens[i].balance;
	// 		} else {
	// 			result.push(tokens[i]);
	// 		}
	// 	}
	// }
	//
	// console.log("Tokens(2): ", tokens);

    return tokens;
}

export async function checkMempool (socketConnect, walletAddresses, address, callback) {
	try {
		var mempool = (await socketConnect.get_mempool(address)).tx;
		console.log("MEMPOOL ARRAY",mempool);
		var mempoolObjects = await createTransactionsFromHistory(socketConnect, walletAddresses, mempool);

		callback(mempoolObjects, false);

		var interval = setInterval(function() {
			socketConnect.get_mempool(address).then((newMempool) => {
				if (JSON.stringify(mempool) != JSON.stringify(newMempool.tx)) {
					createTransactionsFromHistory(socketConnect, walletAddresses, newMempool.tx).then((newMempoolObjects) => {
						callback(newMempoolObjects, true);
						mempool = newMempool.tx;
					})
				}

				if (newMempool.tx.length == 0) {
					clearInterval(interval);
				}
			});
		}, 2000);
	} catch (e) {
		console.log('mempool error: ', e)
		return;
	}

}
